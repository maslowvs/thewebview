<?php
	
	global $config;
	
//	ini_set('display_errors', 1);
//	ini_set('display_startup_errors', 1);
//	error_reporting(E_ALL);
	
	$config = parse_ini_file("config.ini");
	
	include_once "./SDK/DatabaseSchema.php";
	include_once "./SDK/User.php";
	include_once "./SDK/Report.php";
	include_once "./SDK/Source.php";
	include_once "./SDK/HttpUtils.php";
	include_once "./SDK/FileUtils.php";
	include_once "./ic_builder/ICBuilder.php";
