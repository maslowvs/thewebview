<?php
	
	namespace SDK;
	
	use LessQL\Database;
	use PDO;
	
	class DatabaseSchema
	{
		private $pdo;
		private $db;
		
		protected function getPDO(): PDO {
			return $this->pdo;
		}
		
		protected function getDB(): Database {
			return $this->db;
		}
		
		public function __construct()
		{
			global $config;
			
			if (isset($config["sqliteName"])) {
				$sqliteName = $config["sqliteName"];
				
				$this->pdo = new PDO("sqlite:db/$sqliteName.sqlite3");
				$this->db = new Database($this->pdo);
			}
		}
	}