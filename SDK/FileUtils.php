<?php
	
	namespace SDK\User;
	
	class FileUtils
	{
		static function getBytes($val): float|int|string
		{
			$val = trim($val);
			$last = strtolower($val[strlen($val)-1]);
			switch($last) {
				// The 'G' modifier is available since PHP 5.1.0
				case 'g':
					$val *= (1024 * 1024 * 1024); //1073741824
					break;
				case 'm':
					$val *= (1024 * 1024); //1048576
					break;
				case 'k':
					$val *= 1024;
					break;
			}
			
			return $val;
		}
	}