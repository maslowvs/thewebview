<?php
	
	namespace SDK\User;
	
	class HttpUtils
	{
		static function parseHeaders(string $response): array
		{
			$headers = [];
			foreach (explode("\r\n", substr($response, 0, strpos($response, "\r\n\r\n"))) as $i => $line) {
				if ($i === 0) continue;
				list ($key, $value) = explode(': ', $line);
				$headers[$key] = $value;
			}
			
			return $headers;
		}
	}