<?php
	
	namespace SDK\User;
	
	use LessQL\Result;
	use LessQL\Row;
	use SDK\DatabaseSchema;
	
	class Report extends DatabaseSchema
	{
		public function createReport(
			string $packageName,
			?string $owner,
			string $jsonBody
		): null|string
		{
			$row = $this->getDB()->createRow('report', [
				'packageName' => $packageName,
				'data' => $jsonBody,
				'owner' => $owner
			]);
			
			return $row->save()->getId();
		}
		
		public function getReportById(
			string $id
		): null|Row
		{
			return $this
				->getDB()
				->report()
				->where('id', $id)
				->fetch();
		}
		
		public function all(): null|Result
		{
			return $this
				->getDB()
				->report()
				->orderBy('id', 'DESC')
				->paged(25, 1);
		}
	}