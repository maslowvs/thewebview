<?php
	
	namespace SDK\User;
	
	use LessQL\Result;
	use LessQL\Row;
	use PDO;
	use SDK\DatabaseSchema;
	use stdClass;
	
	class Source extends DatabaseSchema
	{
		public function createSource(string $packageName, string $path): Row
		{
			$row = $this->getDB()->createRow('source', [
				'packageName' => $packageName,
				'path' => $path
			]);
			
			return $row->save();
		}
		
		public function getById($id): null|Row
		{
			return $this
				->getDB()
				->source()
				->where('id', $id)
				->fetch();
		}
		
		public function updateAPK($id, string $path): null|bool
		{
			return $this
				->getDB()
				->update("source", [
					"apkPath" => $path
				], [
					"id = '$id'"
				])
				->fetch();
		}
		
		public function all(): null|Result
		{
			return $this
				->getDB()
				->source()
				->orderBy('id', 'DESC')
				->paged(25, 1);
		}
	}