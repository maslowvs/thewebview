<?php
	
	namespace SDK\User;
	
	use LessQL\Row;
	use PDO;
	use SDK\DatabaseSchema;
	use stdClass;
	
	class User extends DatabaseSchema
	{
		public function createUser(string $username, string $password): Row
		{
			$row = $this->getDB()->createRow('user', [
				'username' => $username,
				'password' => password_hash($password, PASSWORD_BCRYPT)
			]);
			
			return $row->save();
		}
		
		public function getUser(string $username, string $password): ?array
		{
			$row = $this
				->getDB()
				->user()
				->where('username', $username)
				->fetch();
			
			if ($row instanceof Row) {
				if (password_verify($password, $row->getData()["password"])) {
					return $row->getData();
				}
			}
			
			return null;
		}
	}