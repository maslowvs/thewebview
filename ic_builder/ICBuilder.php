<?php
	
	use Symfony\Component\Yaml\Yaml;
	
	/**
	 *
	 */
	class ICBuilder
	{
		/**
		 * @param string $name
		 * @return mixed
		 */
		static function readImpl(string $name)
		{
			return Yaml::parseFile(__DIR__ . "/$name/impl.yml");
		}
		
		/**
		 * @param string $name
		 * @param string $path
		 * @return false|string
		 */
		static function readFile(string $name, string $path)
		{
			return file_get_contents(__DIR__ . "/$name/$path");
		}
		
		/**
		 * @param string $name
		 * @param string $path
		 * @return false|string
		 */
		static function readDefault(string $name, string $path)
		{
			return ICBuilder::readFile($name, "default/$path");
		}
		
		/**
		 * @param string $name
		 * @param string $path
		 * @return false|string
		 */
		static function readReplace(string $name, string $path)
		{
			return ICBuilder::readFile($name, "replace/$path");
		}
		
		/**
		 * @param string $body
		 * @param array $param
		 * @return array|string|string[]
		 */
		static function build(string $body, array $param)
		{
			return str_replace(array_keys($param), array_values($param), $body);
		}
		
		/**
		 * @param $name
		 * @return string
		 */
		static function getCacheDir($name = "")
		{
			return __DIR__ . "/../cache/$name";
		}
		
		/**
		 * @param string $dir
		 * @return string
		 */
		static function createCacheDir(string $dir)
		{
			if (!file_exists(__DIR__ . "/../cache/$dir")) {
				mkdir(__DIR__ . "/../cache/$dir");
			}
			
			return __DIR__ . "/../cache/$dir";
		}
		
		/**
		 * @param $dirPath
		 * @return void
		 */
		public static function deleteDir($dirPath): void
		{
			if (! is_dir($dirPath)) {
				throw new InvalidArgumentException("$dirPath must be a directory");
			}
			if (!str_ends_with($dirPath, '/')) {
				$dirPath .= '/';
			}
			$files = glob($dirPath . '*', GLOB_MARK);
			foreach ($files as $file) {
				if (is_dir($file)) {
					self::deleteDir($file);
				} else {
					unlink($file);
				}
			}
			rmdir($dirPath);
		}
		
		/**
		 * @param string $name
		 * @param string $cache
		 * @return string
		 */
		static function copyAllDefaultToCache(string $name, string $cache)
		{
			self::recurseCopy(
				__DIR__ . "/$name/default/",
				$cache
			);
			
			return $name;
		}
		
		/**
		 * @param string $sourceDirectory
		 * @param string $destinationDirectory
		 * @param string $childFolder
		 * @return void
		 */
		static function recurseCopy(
			string $sourceDirectory,
			string $destinationDirectory,
			string $childFolder = ''
		): void {
			$directory = opendir($sourceDirectory);
			
			if (is_dir($destinationDirectory) === false) {
				mkdir($destinationDirectory);
			}
			
			if ($childFolder !== '') {
				if (is_dir("$destinationDirectory/$childFolder") === false) {
					mkdir("$destinationDirectory/$childFolder");
				}
				
				while (($file = readdir($directory)) !== false) {
					if ($file === '.' || $file === '..') {
						continue;
					}
					
					if (is_dir("$sourceDirectory/$file") === true) {
						self::recurseCopy("$sourceDirectory/$file", "$destinationDirectory/$childFolder/$file");
					} else {
						copy("$sourceDirectory/$file", "$destinationDirectory/$childFolder/$file");
					}
				}
				
				closedir($directory);
				
				return;
			}
			
			while (($file = readdir($directory)) !== false) {
				if ($file === '.' || $file === '..') {
					continue;
				}
				
				if (is_dir("$sourceDirectory/$file") === true) {
					self::recurseCopy("$sourceDirectory/$file", "$destinationDirectory/$file");
				}
				else {
					copy("$sourceDirectory/$file", "$destinationDirectory/$file");
				}
			}
			
			closedir($directory);
		}
		
		/**
		 * @param $source
		 * @param $destination
		 * @return bool
		 */
		static function zip($source, $destination)
		{
			if (!extension_loaded('zip') || !file_exists($source)) {
				return false;
			}
			
			$zip = new ZipArchive();
			if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
				return false;
			}
			
			$source = str_replace('\\', '/', realpath($source));
			
			if (is_dir($source) === true) {
				$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
				
				foreach ($files as $file) {
					$file = str_replace('\\', '/', $file);
					
					// Ignore "." and ".." folders
					if (in_array(substr($file, strrpos($file, '/')+1), array('.', '..'))) {
						continue;
					}
					
					$file = realpath($file);
					
					if (is_dir($file) === true) {
						$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
					} elseif (is_file($file) === true) {
						$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
					}
				}
			} elseif (is_file($source) === true) {
				$zip->addFromString(basename($source), file_get_contents($source));
			}
			
			return $zip->close();
		}
		
		/**
		 * @param int $length
		 * @return string
		 */
		static function readableRandomString(int $length = 6): string
		{
			$string = '';
			$vowels = array("a","e","i","o","u");
			$consonants = array(
				'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
				'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
			);
			$max = $length / 2;
			for ($i = 1; $i <= $max; $i++)
			{
				$string .= $consonants[rand(0,19)];
				$string .= $vowels[rand(0,4)];
			}
			return $string;
		}
		
		static function getVariable($value, $variables)
		{
			switch ($value) {
				case "?randomInt":
					return rand(100000, 1000000);
				
				case "?randomWord":
					return self::readableRandomString(rand(10, 18));
					
				case "?randomClassName":
					return "IC" . ucfirst(self::readableRandomString(rand(10, 18)));
			}
			
			return $value;
		}
		
		static function getFormattedString($value, $variables)
		{
			global $variablesMap;
			$variablesMap = [];
			
			array_walk($variables, function(&$a, $b) {
				global $variablesMap;
				
				$variablesMap["$$b"] = $a;
			});
			
			return str_replace(array_keys($variablesMap), array_values($variablesMap), $value);
		}
	}