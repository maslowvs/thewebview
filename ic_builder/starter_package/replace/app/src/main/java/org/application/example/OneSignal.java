package {% packageName %};

import android.app.Application;

public class $OneSignalClassName extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        com.onesignal.OneSignal.initWithContext(this);
        com.onesignal.OneSignal.setAppId("ONESIGNAL_APP_ID");
    }
}
