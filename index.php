<?php
	
	use Bramus\Router\Router;
	use Josantonius\Session\Session;
	use LessQL\Result;
	use SDK\User\HttpUtils;
	use SDK\User\Report;
	use SDK\User\Source;
	use SDK\User\User;
	
	include_once "vendor/autoload.php";
	include_once "Pacman.php";
	
	$report = new Report();
	$session = new Session();
	$user = new User();
	$source = new Source();
	
	if (!$session->isStarted()) {
		$session->start();
	}
	
	$loader = new Twig_Loader_Filesystem('views');
	
	$options = array(
		'strict_variables' => false,
		'debug' => false,
		'cache'=> false
	);
	
	$twig = new Twig_Environment($loader, $options);
	
	$twig->addFilter( new Twig_SimpleFilter('cast_to_array', function ($stdClassObject) {
		$response = array();
		foreach ($stdClassObject as $key => $value) {
			$response[] = array($key, $value);
		}
		return $response;
	}));
	
	$twig->addFilter( new Twig_SimpleFilter('count', function (Result $array) {
		return count($array->fetchAll());
	}));
	
	$twig->addFilter( new Twig_SimpleFilter('json_decode', function (string $data) {
		return json_decode($data);
	}));
	
	$twig->addFilter( new Twig_SimpleFilter('dir_size', function (string $directory) {
		$size = 0;
		foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file){
			$size+=$file->getSize();
		}
		return $size;
	}));
	
	$twig->addFilter( new Twig_SimpleFilter('format_size', function (string $size) {
		$units = explode(' ', 'B KB MB GB TB PB');
		
		$mod = 1024;
		
		for ($i = 0; $size > $mod; $i++) {
			$size /= $mod;
		}
		
		$endIndex = strpos($size, ".")+3;
		
		return substr( $size, 0, $endIndex).' '.$units[$i];
	}));
	
	$twig->addFilter( new Twig_SimpleFilter('to_line', function ($stdClassObject) {
		$number = array_keys((array) $stdClassObject)[0];
		$body = array_values((array) $stdClassObject)[0];
		
		return array($number, $body);
	}));
	
	$router = new Router();
	
	$router->get("/", function () use ($twig, $loader) {
		print $twig->render("index.twig");
	});
	
	$router->get("/builder", function () use ($source, $twig, $loader) {
		print $twig->render("builder.twig", []);
	});
	
	$router->post("/builder", function () use ($source, $twig, $loader) {
		$colors = [
			[ "#ffba08", "#ECAA00" ],
			[ "#f48c06", "#E08106" ],
			[ "#90A955", "#7F954B" ],
			[ "#48cae4", "#42B9D0" ],
			[ "#E5989B", "#D78E91" ],
			[ "#ff595e", "#EB5257" ],
			[ "#FFCA3A", "#F2C037" ],
			[ "#8ac926", "#7CB422" ],
			[ "#1982c4", "#187AB8" ],
			[ "#6a4c93", "#634789" ],
			[ "#e7ad99", "#DAA391" ],
			[ "#240046", "#1C0432" ],
			[ "#ff9e00", "#F19500" ],
			[ "#ff9100", "#EA8500" ],
			[ "#ff7900", "#ED7100" ],
			[ "#ff6d00", "#EA6400" ],
		];
		$color = $colors[array_rand($colors)];
		
		$packageName = $_POST["packageName"];
		$package = explode(".", $packageName);
		$appName = ucfirst($package[2]);
		
		if (count($package) != 3) {
			$packageWord = ICBuilder::readableRandomString(6);
			$packageName = "app.fill.$packageWord";
			$package = explode(".", $packageName);
		}
		
		$variable = [
			"newPackageDir" => "$package[0]/$package[1]/$package[2]"
		];
		
		$impl = ICBuilder::readImpl("starter_package");
		$cache = ICBuilder::createCacheDir($packageName);
	
		ICBuilder::copyAllDefaultToCache("starter_package", $cache);
		
		foreach ($impl["variable"] as $name => $value) {
			$variable[$name] = ICBuilder::getVariable($value, $variable);
		}
		
		foreach ($impl["copy"] as $key => $value)
		{
			$buffer = ICBuilder::readReplace("starter_package", $key);
			
			$build = ICBuilder::build($buffer, [
				"{% packageName %}" => $packageName,
				"{% appName %}" => $appName,
				
				"{% colorLight %}" => mb_strtoupper($color[0]),
				"{% colorDark %}" => mb_strtoupper($color[1]),
			]);
			
			file_put_contents("$cache/$value", $build);
		}
		
		$javaDir = "$cache/app/src/main/java/$package[0]/$package[1]/$package[2]";
		
		if (!file_exists($javaDir)) {
			mkdir("$cache/app/src/main/java/$package[0]");
			mkdir("$cache/app/src/main/java/$package[0]/$package[1]");
			mkdir("$cache/app/src/main/java/$package[0]/$package[1]/$package[2]");
		
			ICBuilder::recurseCopy(
				"$cache/app/src/main/java/" . $impl["project"]["javaDir"],
				$javaDir
			);
		}
		
		foreach ($impl["renameFile"] as $key => $value)
		{
			if (file_exists(ICBuilder::getFormattedString("$cache/$key", $variable)))
			{
				rename(
					ICBuilder::getFormattedString("$cache/$key", $variable),
					ICBuilder::getFormattedString("$cache/$value", $variable)
				);
			}
		}
		
		foreach ($impl['replaceWithVariable'] as $path => $item) {
			$pathName = "$cache/" . ICBuilder::getFormattedString($path, $variable);
			
			if (file_exists($pathName)) {
				file_put_contents(
					$pathName,
					ICBuilder::getFormattedString(file_get_contents($pathName), $variable)
				);
			}
		}
		
		if (file_exists("$cache/app/src/main/java/" . $impl["project"]["javaDirRoot"])) {
			ICBuilder::deleteDir("$cache/app/src/main/java/" . $impl["project"]["javaDirRoot"]);
		}
		
		$cacheZip = ICBuilder::getCacheDir("App$appName.zip");
		$zip = ICBuilder::zip($cache, $cacheZip);
		
		if (file_exists($cache)) {
			system("rm -r $cache");
		}
		
		if ($zip) {
			print $twig->render("builder_build.twig", [
				"url" => str_replace(__DIR__, "", $cacheZip),
				"appName" => $appName,
				"packageName" => $packageName
			]);
		}
		
	});
	
	$router->get("/login", function () use ($twig, $loader) {
		print $twig->render("login.twig");
	});
	
	$router->get("/overview", function () use ($twig, $loader) {
		print $twig->render("overview.twig");
	});
	
	$router->get("/overview/reports", function () use ($report, $twig, $loader) {
		print $twig->render("overview/reports.twig", [
			"reports" => $report->all()
		]);
	});
	
	$router->get("/overview/sources", function () use ($source, $twig, $loader) {
		print $twig->render("overview/sources.twig", [
			"sources" => $source->all()
		]);
	});
	
	$router->get("/overview/live-build", function () use ($twig, $loader) {
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'localhost:4567/getBuildBundles',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
		));
		
		$buildBundles = json_decode(curl_exec($curl));
		
		curl_close($curl);
		
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'localhost:4567/getBuildAPKQueue',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
		));
		
		$buildAPKQueue = json_decode(curl_exec($curl));
		
		curl_close($curl);
		
		print $twig->render("overview/live-build.twig", [
			"buildBundles" => $buildBundles,
			"buildAPKQueue" => $buildAPKQueue
		]);
	});
	
	$router->get("/overview/sources", function () use ($source, $twig, $loader) {
		print $twig->render("overview/sources.twig", [
			"sources" => $source->all()
		]);
	});
	
	$router->get("/build/download/{id}", function ($id) use ($source, $twig, $loader) {
		$sourceData = $source->getById($id)->getData();
		
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'localhost:4567/getBuildJob?path=' . $sourceData["path"],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
		));
		
		$response = json_decode(curl_exec($curl));
		
		if ($response->code == "RO2_BUILD_FIND" && isset($response->path)) {
			if (file_exists($response->path)) {
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename='.basename($response->path));
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				header('Content-Length: ' . filesize($response->path));
				ob_clean();
				flush();
				readfile($response->path);
				exit;
			}
		}
		
		curl_close($curl);
	});
	
	$router->get("/build/{id}", function ($id) use ($source, $twig, $loader) {
		$sourceData = $source->getById($id)->getData();
		
		if (empty($sourceData)) {
			exit(1);
		}
		
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'localhost:4567/getBuildJob?path=' . $sourceData["path"],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
		));
		
		$response = json_decode(curl_exec($curl));
		
		curl_close($curl);
		
		if ($response->code == "RO2_RECORD_IN_QUEUE_NOT_FOUND") {
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'localhost:4567/queueExist?path=' . $sourceData['path'],
				
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
			));
			
			$response = json_decode(curl_exec($curl));
			
			curl_close($curl);
			
			if ($response->code == "RO2_QUEUE_NOT_FOUND") {
				
				$curl = curl_init();
				
				curl_setopt_array($curl, array(
					CURLOPT_URL => 'localhost:4567/buildDebug?path=' . $sourceData['path'] . '&async=yes',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
				));
				
				$response = json_decode(curl_exec($curl));
				
				curl_close($curl);
				
				print $twig->render("build.twig", [
					"id" => $id,
					"type" => "STATUS_STARTED_BUILD"
				]);
				
				exit(0);
				
			} // else q found
			
		} elseif ($response->code == "RO2_BUILD_FIND") {
			$source->updateAPK($id, $response->path);
			
			print $twig->render("build.twig", [
				"id" => $id,
				"type" => "STATUS_BUILD",
				"path" => $response->path
			]);
			
			exit(0);
		} else {
			print $twig->render("build.twig", [
				"id" => $id,
				"type" => "STATUS_ERROR"
			]);
			
			exit(0);
		}
	});
	
	$router->get("/check", function () use ($twig, $loader) {
		print $twig->render("check.twig", [
			"post_max_size" => ini_get("post_max_size")
		]);
	});
	
	$router->get("/report/{id}", function ($id) use ($report, $twig, $loader) {
		$data = $report->getReportById($id)->getData()["data"];
		$structure = json_decode($data);
		
		print $twig->render("report.twig", [
			"data" => $structure,
			"isSuccess" => count($structure->files) == 0
		]);
	});
	
	$router->get("/clean/{id}", function ($id) use ($report, $twig, $loader) {
		print $twig->render("clean.twig", [
			"id" => $id
		]);
	});
	
	$router->post("/login", function () use ($session, $user, $twig, $loader) {
		$username = $user->getUser($_POST['email'], $_POST['password']);
		
		if (is_array($username)) {
			$session->set("username", $username["username"]);
			$session->set("password", $username["password"]);
			
			header("Location: /overview", true, 301);
			exit(0);
		} else {
			header("Location: /login", true, 301);
			exit(0);
		}
	});
	
	$router->post("/upload", function () use ($source, $report, $twig) {
		if(isset($_FILES['file'])) {
			$file_name = $_FILES['file']['name'];
			$file_tmp = $_FILES['file']['tmp_name'];
			$type = $_FILES['file']['type'];
			$size = $_FILES['file']['size'];
			$path = $_FILES['file']['name'];
			$error = $_FILES['file']['error'];
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			
			$allowExt = [
				"rar",
				"zip"
			];
			
			$allowType = [
				"application/zip",
				"application/rar",
				"application/x-rar-compressed",
				"application/octet-stream",
				"application/x-zip-compressed",
				"multipart/x-zip",
			];
			
			if (!in_array($ext, $allowExt) || !in_array($type, $allowType)) {
				print $twig->render("upload.twig", [
					"hasError" => "EXT_NOT_FOUND",
					"currentExt" => $ext,
					"currentContentType" => $type,
					"allowExt" => $allowExt,
					"phpError" => $error
				]);
				
				exit(0);
			}
			
			move_uploaded_file($file_tmp, "cache/$file_name");
			
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'localhost:4567/upload',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_HEADER => 1,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => array('file' => new CURLFILE("cache/$file_name")),
			));
			
			$response = curl_exec($curl);
			
			$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
			$header = substr($response, 0, $header_size);
			$body = substr($response, $header_size);
			
			curl_close($curl);
			
			if ($header != "" || count(HttpUtils::parseHeaders($header)) > 0) {
				if (isset(json_decode($body)->path)) {
					$path = json_decode($body)->path;
					
					if (!empty($path)) {
						$curl = curl_init();
						$query = http_build_query([
							"path" => $path
						]);
						
						curl_setopt_array($curl, array(
							CURLOPT_URL => "localhost:4567/analyze?$query",
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => '',
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 0,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => 'GET',
							CURLOPT_POSTFIELDS => array(),
						));
						
						$response = curl_exec($curl);
						
						curl_close($curl);
						
						$reportData = json_decode($response);
						
						if (isset($reportData->packageName) && isset($reportData->files)) {
							$sourceId = $source->createSource($reportData->packageName, $path)->getId();
							
							$reportId = $report->createReport(
								$reportData->packageName,
								null,
								$response
							);
							
							unlink("cache/$file_name");
							
							if (count($reportData->files) == 0) {
								header("Location: /clean/$sourceId");
							} else {
								header("Location: /report/$reportId");
							}
							
							die();
						}
						
					}
				}
			} else {
				print $twig->render("upload.twig", [
					"hasError" => "RO2_UPLOAD_ERROR",
					"fileSize" => $size,
					"phpError" => $error
				]);
			}
		}
		
		print $twig->render("upload.twig");
	});
	
	$router->get("/upload", function () {
		header("Location: /check", true, 301);
		exit(0);
	});
	
	if (isset($_SERVER['REQUEST_METHOD'])) {
		$router->run();
	} else {
		$parser = new Console_CommandLine();
		$parser->description = 'A CLI toolbox';
		$parser->version = '1.0.0';
		
		$parser->addOption('user_create', array(
			'long_name'   => '--user_create',
			'description' => 'Create new user',
			'help_name'   => 'new@company.com',
			'action'      => 'StoreString'
		));
		
		$parser->addOption('user_create_by_name', array(
			'long_name'   => '--user_create_by_name',
			'description' => 'Create new user with email by name',
			'help_name'   => 'new',
			'action'      => 'StoreString'
		));
		
		try {
			$result = $parser->parse();

			if (!empty($result->options['user_create'])) {
				$password = substr(password_hash(rand(0, 10000000), PASSWORD_BCRYPT), 0, 8);
				
				print "\nUsername: " . $result->options['user_create'];
				print "\nPassword: $password";
				
				$operation = $user->createUser($result->options['user_create'], $password);
				
				print  "\nNew user ID: " . $operation->getId();
			}
			
			if (!empty($result->options['user_create_by_name'])) {
				$password = substr(password_hash(rand(0, 10000000), PASSWORD_BCRYPT), 0, 8);
				$username = $result->options['user_create_by_name'];
				
				print "\nUsername: " . "$username@thewebview.lol";
				print "\nPassword: $password";
				
				$operation = $user->createUser("$username@thewebview.lol", $password);
				
				print  "\nNew user ID: " . $operation->getId();
			}
		} catch (Exception $exc) {
			$parser->displayError($exc->getMessage());
		}
	}
	